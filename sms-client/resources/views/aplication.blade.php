<!doctype html>

<!-- IMPORTANT: Declare your Angular App -->
<html lang="en" ng-app="sms-client">
<head>
	<meta charset="UTF-8">
	<title>Korra - Earth | Fire | Air | Water</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="libraries/loading-bar.min.css" type="text/css" rel="stylesheet" />
    <link href="css/app.css" type="text/css" rel="stylesheet" />
    <script>
        var csrf_token = "{{ csrf_token() }}";
    </script>
</head>
<body>

<!-- IMPORTANT: Main Content View used by AngularJS ui-router --->
<div class="Main" ui-view="main"></div>
<footer>
    <div class="container">

    </div>
</footer>
<!-- IMPORTANT: Load all the Angular Magic -->
<script type="text/javascript" src="//cdn.jsdelivr.net/angularjs/1.3.9/angular.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/restangular/latest/restangular.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/angular.ui-router/0.2.13/angular-ui-router.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/lodash/3.0.0/lodash.compat.min.js"></script>
<script type="text/javascript" src="libraries/loading-bar.min.js"></script>

<!-- IMPORTANT: Load your Angular App -->
<script type="text/javascript" src="/Angular/app.js"></script>

</body>
</html>